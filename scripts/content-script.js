'use strict';

// reads and writes to web pages
// communicates with the parent extension via messages
// can store values using the storage API

var triggerWords = []
let mode = 'remove'

function getRegex () {
  return RegExp(triggerWords.join('|'), 'i')
}

// document.addEventListener('DOMContentLoaded', () => {
//   console.log('Loaded DOM')
// });

// use DOM tree walker with filters 
function getAllTextNodes () {
  var regex = getRegex()
  var nodeList = []
  var walk = document.createTreeWalker(
    document.body,
    NodeFilter.SHOW_TEXT,
    { acceptNode: (node) => {
      return !!node.textContent && node.textContent.match(regex) !== null
    } },
    false
  )
  var currentNode = walk.nextNode()

  while(currentNode) {
    nodeList.push(currentNode);
    currentNode = walk.nextNode();
  }

  return nodeList
}

// nodeType nodeName tagName
// 1 DIV DIV
// 1 IMG IMG
// 3 #text undefined
// 1 SCRIPT SCRIPT

function getAllImageNodes () {
  var regex = getRegex()
  var nodeList = []
  const imgs = document.getElementsByTagName('img') // HTMLCollection

  for(let i=0; i<imgs.length; i++) {
    let node = imgs.item(i)
    if (!!node.title && node.title.match(regex) !== null ||
        !!node.alt && node.alt.match(regex) !== null
     ) {
      nodeList.push(node)
    }
  }
  return nodeList
}


function scrape () {
  if (!triggerWords.length) {
    return
  }

  const textNodes = getAllTextNodes()
  const imageNodes = getAllImageNodes()

  censor(textNodes.concat(imageNodes))
}


var excludedNodeTypes = ['SCRIPT', 'BODY']

const censorModes = {
  remove: 'remove',
  blackout: 'blackout',
}

const censorStyle = {
  border: '2px solid blue',
  backgroundColor: 'currentcolor',
}

const findTopElement = (node) => {
  let nextNode = node
  while (
    !excludedNodeTypes.includes(nextNode.nodeName) &&
    nextNode.innerText === node.innerText
  ) {
    nextNode = nextNode.parentNode
  }
  return nextNode
}

function censor (nodes) {
  console.log('censoring...', nodes.length)
  nodes.forEach((node) => {
    const parent = findTopElement(node)
    if (mode === censorModes.remove &&
      !excludedNodeTypes.includes(parent.nodeName) &&
      !excludedNodeTypes.includes(node.nodeName)
    ) {
      parent.remove()
    } else if (node.tagName === 'IMG') {
      node.remove()
    } else {
      node.style = censorStyle
      Object.entries(censorStyle)
        .forEach(([item, style]) => parent.style[item] = style)

        parent.style.border = '2px solid red'
    }
  })
}


// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restoreOptions() {
  chrome.storage.sync.get({
    triggerWords: [],
    mode: 'remove',
  }, (items) => {
    triggerWords = items.triggerWords
    mode = items.mode
    scrape()
  })
}

function updateTriggerWords (newWords) {
  triggerWords = newWords
  scrape()
}

chrome.storage.onChanged.addListener((changes, namespace) => {
  for (var key in changes) {
    var storageChange = changes[key];
    console.log(
      'Storage key', key, 'in namespace',  namespace,
      'changed. Old value was', storageChange.oldValue,
      'new value is', storageChange.newValue
    )
    if (key === 'triggerWords') {
      updateTriggerWords(storageChange.newValue || [])
    }
  }
});

const updateMode = (value) => {
  mode = value
  if (value === censorModes.blackout) {
    window.location.reload();
  } else {
    scrape()
  }
}

console.log(chrome.runtime.onMessage.addListener)

// listen to message
chrome.runtime.onMessage.addListener(
  (request, sender, sendResponse) => {
    console.log(request, sender)

    // do something...
    if (request.mode) {
      updateMode(request.mode)
    }

    return true
  }
)

// send message
// (async () => {
//   const response = await chrome.runtime.sendMessage({ greeting: 'CS' })
//   console.log(response)
// })


// console.log(chrome.activeTab)

// loading new items
document.addEventListener('DOMNodeInserted', scrape)

restoreOptions()
