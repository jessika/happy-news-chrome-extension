'use strict';

var ME = 'popup'
var currentTab
var triggerWords = []

document.addEventListener('DOMContentLoaded', () => {
  document.getElementById('save').addEventListener('click', saveOption)

  const list = document.getElementsByName('mode')

  list.forEach(e => e.addEventListener('change', (ev) => {
    const mode = document.querySelector('input[name="mode"]:checked').value
    console.log('list change', e, ev.target.value, mode)
    sendMessage(ev.target.value)
  }))

  restoreOptions()
});

const selectMode = (mode) => {
  const list = document.getElementsByName('mode')
  for (let i=0; i<list.length; i++) {
    if (list.item(i).value === mode) {
      document.getElementById(mode).setAttribute('checked', true)
    }
  }
}

class ListItem extends HTMLLIElement {
  delete () {
    deleteOption(this.id)
  }

  set id (id) {
    this.setAttribute('id', id)
    this.appendChild(document.createTextNode(id))
    this.button.setAttribute('id', id)
    this.appendChild(this.button)
  }

  constructor () {
    super()
    this.style = {
      'display': 'flex',
      'justify-content': 'space-between'
    }
    this.class = 'word-list-item'
    this.button = document.createElement('button')
    this.button.addEventListener('click', this.delete)  
    this.button.textContent = 'remove'
  }
}

customElements.define('list-element', ListItem, { extends: 'li' })

// chrome.storage.sync.clear()

function saveOption() {
  var value = document.getElementById('text').value;
  if (!!value) {
    chrome.storage.sync.get({
      triggerWords: []
    }, function(items) {
      saveWords(items.triggerWords.concat(value), () => {
        document.getElementById('text').value = ''
        addListItem(value)
      })
    })
  }
}

function deleteOption (value) {
  console.log('deleting', value)
  chrome.storage.sync.get({
    triggerWords: []
  }, (items) => {
    saveWords(items.triggerWords.filter((item) => item !== value), () => {
      console.log('removed', value)
      document.getElementById(value).remove()
    })
  })
}

// Saves options to chrome.storage

const saveSetting = (option, value, cb) => {
  console.log('saving', option, value)
  chrome.storage.sync.set({ [option]: value }, () => {
    if (cb) cb()
  });
}

const saveWords = (triggerWords, cb)  => {
  saveSetting('triggerWords', triggerWords, cb)
}

const saveMode = (mode, cb) => {
  saveSetting('mode', mode, cb)
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restoreOptions() {
  chrome.storage.sync.get({ triggerWords: [], mode: 'remove' }, (items) => {
    triggerWords = items.triggerWords

    items.triggerWords.forEach(item => {
      addListItem(item)
    })

    selectMode(items.mode)
  })
}

function addListItem (item) {
  console.log('adding to list', item)
  var list = document.getElementById('trigger-words')
  var li = document.createElement('li', { is: 'list-element'})
  li.id = item
  list.appendChild(li)
}

const changeMode = (mode) => {
  saveMode(mode, sendMessage(mode))
}

// send message to content script
async function sendMessage (value) {
  const [tab] = await chrome.tabs.query({active: true, lastFocusedWindow: true});
  console.log(tab)
  const res = await chrome.tabs.sendMessage(tab.id, { mode: value }) //, {}, (res) => {
}
