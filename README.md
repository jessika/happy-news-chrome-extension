

## Activate

manage chrome extensions > enable dev mode > load unpacked > select folder

## add icon
/images/icon-16.png etc.

## Structure - pages

### The popup
Many extensions use a popup (popup.html) to provide functionality, such as displaying a list of tabs, or additional information regarding the current tab. Users can easily find it by clicking on the extension toolbar icon. When the user navigates away it will automatically close.
### The options page
The options page (options.html) provides a way for users to customize an extension, such as choosing which sites the extension will run on. Users can access the options page in several ways as described in Finding the options page.
### Side panels
A side panel (sidepanel.html) can be used to assist users throughout their browsing journey. Users can find extension side panels by navigating to Chrome's side panel UI or by clicking the extension toolbar icon. Side panels can be configured to only be displayed on specific sites.

    "default_icon": {
      "32": "icons/icon-32.png"
    }
